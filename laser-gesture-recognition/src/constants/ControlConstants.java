/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author Aryan
 */
public interface ControlConstants {
    int MUSIC_PLAYER = 1;
    int POWER_POINT_PRESENTATION = 2;
    int LEFT = 3;
    int RIGHT = 4;
    int UP = 5;
    int DOWN = 6;
    int NONE = 99999;
}
