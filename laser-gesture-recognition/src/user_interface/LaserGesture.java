/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user_interface;

import constants.ControlConstants;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import utilities.ImageProcessor;

/**
 *
 * @author Aryan
 * This is the background thread class which takes the application to be controlled i.e.either the music player or the powerpoint.
 * The basic logic for processing the gesture is pretty simple.
 * We simply calculate the the displacement of the laser point of both X-Axis and Y-Axis.i.e (x2-x1 and y2-y1)
 * Then further we check whether the displacement is greater than the minimum threshold value.
 * And after finding the direction with the help of the robot class we press the appropriate key as per the direction and current application.
 * For understanding further details about this process refer the code below.
 */
public class LaserGesture implements Runnable{
    private JFrame frame;
    private JLabel imageLabel;
    private boolean isPreviousPoint = false;
    private int previousX,previousY;
    private ImageIcon imageIcon;
    private Robot robot;
    private static int application = ControlConstants.NONE;
    private Thread t;

    static{System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
    
    /**
     * This constructor creates the thread which continously monitor the screen and capture points and detects the direction!
     * @param application : The application that needs to be controlled by laser.
     */
    public LaserGesture(int application){
        this.application = application;
        t = new Thread(this);
        try {
            robot = new Robot();
        } catch (AWTException ex) {
            System.out.println("Exception in creating the robot object!!!");
        }
        t.start();
    }
    public void run(){
        initGUI();
        runMainLoop();
    }
        
    private void initGUI() {
       frame = new JFrame("Camera Input Example");
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setSize(400,400);
       imageLabel = new JLabel();
       frame.add(imageLabel);
       frame.setVisible(true);
    }
    
    /**
     *
     * @param newApplication : It is the new application to be set.i.e.either the music player or the powerpoint
     */
    public static void setApplication(int newApplication){
        application = newApplication;
    }
    
    /**
     *
     * @return : Constant value of the current application.
     */
    public static int getApplication(){
        return application;
    }
    
    private void runMainLoop() {
        //Initliaizing the matrix in which the images captured from webcam would be stored.
        Mat webcamMatImage = new Mat();
        Image tempImage;
        
        //Intitializing videoCapture object that would capture the images from default in-built webcam.   	
        VideoCapture capture = new VideoCapture(0);
        
        //Setting the resoultion of the webcam.
        capture.set(Videoio.CAP_PROP_FRAME_WIDTH,1366);
        capture.set(Videoio.CAP_PROP_FRAME_HEIGHT,768);
        if( capture.isOpened()){
            while (true){
                capture.read(webcamMatImage);
                if( !webcamMatImage.empty() ){
                    tempImage= ImageProcessor.toBufferedImage(webcamMatImage);
                    processDirection(webcamMatImage);
                    imageIcon = new ImageIcon(tempImage, "Captured video");
                    frame.pack(); //this will resize the window to fit the image
                }
                else{
                    System.out.println(" -- Frame not captured -- Break!");
                    break;
                }
            }
        }
        else{
            System.out.println("Couldn't open capture.");
        }
    }
    /**
     * In this method we are identifying the two points and x,y coordinate of two points are subtracted to get the xDiff(x2-x1) and yDiff(y2-y1).
     * And according to the xDiff and yDiff identified the direction and passed to the appropriateKeyPress method for the key pressing according to the direction!!!
     * @param webcamMatImage : It is the matrix of the image that is captured by the webcam.
     */
    private void processDirection(Mat webcamMatImage){
        Mat destinationImage = new Mat();
        Imgproc.threshold(webcamMatImage, destinationImage,150,255,Imgproc.THRESH_BINARY);
        //Above are the grayscale values 150 and 255
        //The thrid parameter is the threahhold value if the given pixel i.e grayscale value is greater than take maxvalue which is 4 
        //parameter else take zero according to the Threah Binary
        Mat temp = new Mat();
        
        //Extracting only red color objects from the image.
        Core.inRange(destinationImage, new Scalar(0,0,250), new Scalar(0,0,255), temp);
        imageLabel.setIcon(new ImageIcon(ImageProcessor.toBufferedImage(temp)));
        System.out.println("Set done");
        //      A Scalar object representing the color of the circle. (BGR)
        //      The second and the third parameter are lower and uper bounds if between them then include it in the destinationMat otherwise black(0,0,0)
        
        //Converting the matrix to byte array
        byte[] buffer =  new byte[(int)(temp.total() * temp.elemSize())];
        temp.get(0, 0, buffer);
        
        if(getXCoordinate(buffer,temp.height(),temp.width()) != -1){
            //If we get x then there will rbe y
            if(!isPreviousPoint){
                this.isPreviousPoint = true;
                this.previousX = getXCoordinate(buffer,temp.height(),temp.width());
                this.previousY = getYCoordinate(buffer,temp.height(),temp.width());
                
            }else{
                int xDiff = getXCoordinate(buffer,temp.height(),temp.width()) - this.previousX;
                int yDiff = getYCoordinate(buffer,temp.height(),temp.width()) - this.previousY;
                if(xDiff != 0 || yDiff !=0){
                    
                    /*if(xDiff >= 400 && yDiff >=400){
                            if(xDiff > yDiff){UP}
                            else{}
                        }
                    */
//                    if(xDiff >= 400) {
//                        System.out.println("Right");
//                        
//                        appropriateKeyPress(ControlConstants.RIGHT);
////                            robot.keyPress(KeyEvent.VK_R);
//                        isPreviousPoint = false;
//                    }
//                    else if( xDiff <= -400) {
//                        System.out.println("Left");
//                        appropriateKeyPress(ControlConstants.LEFT);
//
////                            robot.keyPress(KeyEvent.VK_L);
//                        isPreviousPoint = false;
//                    }else if(yDiff >= 400){
//                        System.out.println("Up");//y1 is bigger than y2(y2-y1)
//                    }else if(yDiff <= -400){
//                        System.out.println("Down");
//                    }
//------------------------------------------------------------------------------------------------------------------------------

//                    if(xDiff >= 200 && yDiff >=200){
//                        if(xDiff > yDiff){
//                          robot.keyPress(KeyEvent.VK_R);
//                        }else{
//                          robot.keyPress(KeyEvent.VK_D);
//                        }
//                    }else if(xDiff <= -200 && yDiff <= -200){
//                        if(xDiff > yDiff){
//                          robot.keyPress(KeyEvent.VK_L);
//                        }else{
//                          robot.keyPress(KeyEvent.VK_U);
//                        }
                    if(xDiff >= 200 && Math.abs(yDiff)<50){
                        robot.keyPress(KeyEvent.VK_R);
                        appropriateKeyPress(ControlConstants.RIGHT);
                        this.isPreviousPoint = false;
                    }else if(xDiff <= -200 && Math.abs(yDiff)<50){
                        robot.keyPress(KeyEvent.VK_L);
                        appropriateKeyPress(ControlConstants.LEFT);
                        this.isPreviousPoint = false;
                    }else if(yDiff >= 200 && Math.abs(xDiff)<50){
                        robot.keyPress(KeyEvent.VK_D);
                        appropriateKeyPress(ControlConstants.DOWN);
                        this.isPreviousPoint = false;
                    }else if(yDiff <= -200 && Math.abs(xDiff)<50){
                        robot.keyPress(KeyEvent.VK_U);
                        appropriateKeyPress(ControlConstants.UP);
                        this.isPreviousPoint = false;  
                    }
                }
                this.isPreviousPoint = false;
            }
        }
    }
    
    /**
     * This function takes the direction as the input and press the appropriate key for that direction by using the Robot class.
     * @param : direction : The constant of the direction which is processed i.e either UP,DOWN,RIGHT or LEFT 
     * @return : Nothing
     */
    private void appropriateKeyPress(int direction){
        if(application == ControlConstants.MUSIC_PLAYER){
            if(direction == ControlConstants.UP){
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_P);
                robot.keyRelease(KeyEvent.VK_CONTROL);
            }else if(direction == ControlConstants.DOWN){
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_P);
                robot.keyRelease(KeyEvent.VK_CONTROL);
            }else if(direction == ControlConstants.LEFT){
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_B);
                robot.keyRelease(KeyEvent.VK_CONTROL);
            }else if(direction == ControlConstants.RIGHT){
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_F);
                robot.keyRelease(KeyEvent.VK_CONTROL);
            }
        }
        else if(application == ControlConstants.POWER_POINT_PRESENTATION){
            if(direction == ControlConstants.UP){
                robot.keyPress(KeyEvent.VK_F5);
            }else if(direction == ControlConstants.DOWN){
                robot.keyPress(KeyEvent.VK_ESCAPE);
            }else if(direction == ControlConstants.LEFT){
                robot.keyPress(KeyEvent.VK_LEFT);
            }else if(direction == ControlConstants.RIGHT){
                robot.keyPress(KeyEvent.VK_RIGHT);
            }
        }
    }
    
    /**
     * This function traverses the given matrix to check the white pixels and returns the X-coordinate of those pixels.
     * @param buffer : The byte array of the matrix in which pointer needs to extracted.
     * @param matrixHeight : The height of the matrix in which pointer needs to extracted.
     * @param matrixWidth : The width of the matrix in which pointer needs to extracted.
     * @return : X Coordinate of the pixel if found otherwise it returns -1;
     */
    private int getXCoordinate(byte[] buffer,int matrixHeight,int matrixWidth) {
        for(int i=0;i<matrixHeight;i++) {
            for(int j=0;j<matrixWidth;j++) {
                if(buffer[(matrixWidth*i) + j] == -1) {
                    return j;
                }
            }
        }
        return -1;
    }
    
    /**
     * This function traverses the given matrix to check the white pixels and returns the Y-coordinate of those pixels.
     * @param buffer : The byte array of the matrix in which pointer needs to extracted.
     * @param matrixHeight : The height of the matrix in which pointer needs to extracted.
     * @param matrixWidth : The width of the matrix in which pointer needs to extracted.
     * @return : Y Coordinate of the pixel if found otherwise it returns -1;
     */
    private int getYCoordinate(byte[] buffer,int matrixHeight,int matrixWidth) {
        for(int i=0;i<matrixHeight;i++) {
            for(int j=0;j<matrixWidth;j++) {
                if(buffer[(matrixWidth*i) + j] == -1) {
                    return i;
                }
            }
        }
        return -1;
    }
}
