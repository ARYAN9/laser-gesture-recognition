/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import constants.ControlConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;
import user_interface.LaserGesture;
import user_interface.MainUI;

/**
 *
 * @author Aryan
 */
public class ApplicationControlActionListener implements ActionListener{
    
    private MainUI refObj;
    private JRadioButton refRbt;

    public ApplicationControlActionListener(MainUI refObj, JRadioButton refRbt){
        this.refObj = refObj;
        this.refRbt = refRbt;
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(refRbt == refObj.getRbtPpt()){
            //Radio Button of powerpoint is selected
            if(LaserGesture.getApplication() == ControlConstants.NONE)
                new LaserGesture(ControlConstants.POWER_POINT_PRESENTATION);
            else
                LaserGesture.setApplication(ControlConstants.POWER_POINT_PRESENTATION);
        }else if(refRbt == refObj.getRbtMusicPlayer()){
            //Radio Button of music player is selected
            if(LaserGesture.getApplication() == ControlConstants.NONE)
                new LaserGesture(ControlConstants.MUSIC_PLAYER);
            else
                LaserGesture.setApplication(ControlConstants.MUSIC_PLAYER);
        }
    }
}
